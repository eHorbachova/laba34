﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace lab.rozetka.Models
{
    public class Commodity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual List<CommodityParameter> Parameters { get; set; }

    }

    public class CommodityParameter
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public double Value { get; set; }

        [ForeignKey("Commodity")]
        public int CommodityId { get; set; }

        [XmlIgnore]
        public virtual Commodity Commodity { get; set; }
    }
}
