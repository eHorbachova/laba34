﻿using lab.rozetka.DataAccess;
using lab.rozetka.Models;
using Microsoft.AspNetCore.Mvc;

namespace lab.rozetka.Controllers
{
    [Route("api/entities")]
    [ApiController]
    public class EntitiesCreateController : ControllerBase
    {
        [HttpPost("vendors")]
        public IActionResult Post([FromBody] Vendor body) => Add(body);

        [HttpPost("commodities")]
        public IActionResult CreateCommodities([FromBody] Commodity body) => Add(body);

        [HttpPost("commodities-parameter")]
        public IActionResult CreateCommotitiesParameters([FromBody] CommodityParameter body) => Add(body);

        [HttpPost("currencies")]
        public IActionResult CreateCurrencies([FromBody] Currency body) => Add(body);

        [HttpPost("offers")]
        public IActionResult CreateOffers([FromBody] Offer body) => Add(body);

        [HttpPost("prices")]
        public IActionResult CreatePrices([FromBody] Price body) => Add(body);

        [HttpPost("shops")]
        public IActionResult CreateShops([FromBody] Shop body) => Add(body);

        private IActionResult Add<TEntity>(TEntity body)
        {
            using var db = new ApplicationContext();

            db.Add(body);
            db.SaveChanges();

            return Ok();
        }
    }
}
