﻿namespace lab.rozetka.Models
{
    public class Currency
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Sign { get; set; }

        public int Rate { get; set; }
    }
}
