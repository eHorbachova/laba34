﻿using lab.rozetka.Models;
using Microsoft.EntityFrameworkCore;

namespace lab.rozetka.DataAccess
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Commodity> Commodities { get; set; }
        public DbSet<CommodityParameter> CommodityParameters { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<Shop> Shops { get; set; }
        public DbSet<Vendor> Vendors { get; set; }

        public ApplicationContext()
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=labvntu;Trusted_Connection=True;");
        }

    }
}
