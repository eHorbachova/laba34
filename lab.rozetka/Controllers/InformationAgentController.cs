﻿using lab.rozetka.DataAccess;
using lab.rozetka.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace lab.rozetka.Controllers
{
    [ApiController]
    [Route("api")]
    public class InformationAgentController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<InformationAgentController> _logger;

        public InformationAgentController(ILogger<InformationAgentController> logger)
        {
            _logger = logger;
        }

        [HttpGet("vendors")]
        public IEnumerable<Vendor> Get()
        {
            var vendors = new List<Vendor>();
            using (var db = new ApplicationContext())
            {
                vendors = db.Vendors.ToList();
            }

            return vendors;
        }

        [HttpGet("action")]
        public IActionResult GetAction()
        {
            using (var db = new ApplicationContext())
            {
                var parameter = new CommodityParameter
                {
                    Name = "Test1",
                    Value = 10.1
                };

                db.CommodityParameters.Add(parameter);
                db.SaveChanges();
            }

            return Ok("Action");
        }
    }
}
