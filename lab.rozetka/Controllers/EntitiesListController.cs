﻿using System.Collections.Generic;
using System.Linq;
using lab.rozetka.DataAccess;
using lab.rozetka.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace lab.rozetka.Controllers
{
    [Route("api/entities")]
    [ApiController]
    public class EntitiesListController : ControllerBase
    {
        [HttpGet("vendors")]
        public IEnumerable<Vendor> Get()
        {
            using var db = new ApplicationContext();
            return db.Vendors.ToList();
        }

        [HttpGet("commodities")]
        public IActionResult GetCommodities()
        {
            using var db = new ApplicationContext();
            var dbResult = db.Commodities.Include(t => t.Parameters).ToList();

            return Ok(dbResult);
        }

        [HttpGet("commodities-parameter")]
        public IEnumerable<CommodityParameter> GetCommodityParameter()
        {
            using var db = new ApplicationContext();
            return db.CommodityParameters.ToList();
        }

        [HttpGet("currencies")]
        public IEnumerable<Currency> GetCurrency()
        {
            using var db = new ApplicationContext();
            return db.Currencies.ToList();
        }

        [HttpGet("offers")]
        public IEnumerable<Offer> GetOffer()
        {
            using var db = new ApplicationContext();

            return db.Offers
                .Include(x => x.Commodity)
                .ThenInclude(c => c.Parameters)
                .Include(x => x.Price)
                .Include(x => x.Vendor).ToList();
        }

        [HttpGet("prices")]
        public IEnumerable<Price> GetPrice()
        {
            using var db = new ApplicationContext();
            return db.Prices.ToList();
        }

        [HttpGet("shops")]
        public IEnumerable<Shop> GeShopt()
        {
            using var db = new ApplicationContext();
            return db.Shops.ToList();
        }
    }
}
