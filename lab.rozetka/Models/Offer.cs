﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace lab.rozetka.Models
{
    public class Offer
    {
        public int Id { get; set; }

        [ForeignKey("Vendor")]
        public int VendorId { get; set; }

        [ForeignKey("Commodity")]
        public int CommodityId { get; set; }

        [ForeignKey("Price")]
        public int PriceId { get; set; }

        [XmlIgnore]
        public virtual Vendor Vendor { get; set; }

        [XmlIgnore]
        public virtual Commodity Commodity { get; set; }

        [XmlIgnore]
        public virtual Price Price { get; set; }
    }
}
