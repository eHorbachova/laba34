﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace lab.rozetka.Models
{
    public class Vendor
    {
         public int Id { get; set; }

        public string Name { get; set; }

        [XmlIgnore]
        public virtual List<Offer> Offers { get; set; }
    }
}
