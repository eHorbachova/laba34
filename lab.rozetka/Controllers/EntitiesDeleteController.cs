﻿using lab.rozetka.DataAccess;
using Microsoft.AspNetCore.Mvc;

namespace lab.rozetka.Controllers
{
    [Route("api/entities")]
    [ApiController]
    public class EntitieDeleteController : ControllerBase
    {
        [HttpDelete("vendors/{id}")]
        public void Delete([FromRoute] int id)
        {
            using var db = new ApplicationContext();

            var enitity = db.Vendors.Find(id);

            db.Remove(enitity);
            db.SaveChanges();

        }

        [HttpDelete("commodities/{id}")]
        public void DeleteCommodities([FromRoute] int id)
        {
            using var db = new ApplicationContext();

            var enitity = db.Commodities.Find(id);

            db.Remove(enitity);
            db.SaveChanges();

        }

        [HttpDelete("commodities-parameter/{id}")]
        public void DeleteCommotitiesParameters([FromRoute] int id)
        {
            using var db = new ApplicationContext();

            var enitity = db.CommodityParameters.Find(id);

            db.Remove(enitity);
            db.SaveChanges();

        }

        [HttpDelete("currencies/{id}")]
        public void DeleteCurrencies([FromRoute] int id)
        {
            using var db = new ApplicationContext();

            var enitity = db.Currencies.Find(id);

            db.Remove(enitity);
            db.SaveChanges();

        }

        [HttpDelete("offers/{id}")]
        public void DeleteOffers([FromRoute] int id)
        {
            using var db = new ApplicationContext();

            var enitity = db.Offers.Find(id);

            db.Remove(enitity);
            db.SaveChanges();

        }

        [HttpDelete("prices/{id}")]
        public void DeletePrices([FromRoute] int id)
        {
            using var db = new ApplicationContext();

            var enitity = db.Prices.Find(id);

            db.Remove(enitity);
            db.SaveChanges();

        }

        [HttpDelete("shops/{id}")]
        public void DeleteShops([FromRoute] int id)
        {
            using var db = new ApplicationContext();

            var enitity = db.Shops.Find(id);

            db.Remove(enitity);
            db.SaveChanges();

        }
    }
}
