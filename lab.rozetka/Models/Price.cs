﻿using System.ComponentModel.DataAnnotations.Schema;

namespace lab.rozetka.Models
{
    public class Price
    {
        public int Id { get; set; }

        [ForeignKey("Currency")]
        public int CurrencyId { get; set; }

        public double Value { get; set; }

        public virtual Currency Currency { get; set; }

    }
}
